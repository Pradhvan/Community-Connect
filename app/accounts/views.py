# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.shortcuts import render
from django.http import HttpResponse
from django.contrib.auth import login, authenticate
from .forms import LoginForm, UserRegistrationForm
from django.contrib.auth.decorators import login_required
from django.conf import settings

@login_required(login_url='/accounts/login/')
def dashboard(request):
    return render(request,'accounts/dashboard.html',{'section':'dashboard', "community": settings.COMMUNITY })


def user_login(request):
    if request.method == 'POST':
        form = LoginForm(request.POST)
        if form.is_valid():
            cd = form.cleaned_data
            user = authenticate(
                username=cd['username'], password=cd['password'])
            if user is not None:
                if user.is_active:
                    login(request, user)
                    return HttpResponse('Authenticated successfully')
                else:
                    return HttpResponse('Disabled account')
            else:
                return HttpResponse('invalid login')
    else:
        form = LoginForm()
        return render(request, 'accounts/login.html', { 'form':form, "community": settings.COMMUNITY })


def register(request):
    if request.method == 'POST':
        user_form = UserRegistrationForm(request.POST)
        if user_form.is_valid():
            new_user = user_form.save(commit=False)
            new_user.set_password(user_form.cleaned_data['password'])
            new_user.save()
            return render(request,'accounts/register_done.html',{'new_user':new_user})
        else:
            return render(request,'accounts/register.html', {'user_form':user_form, "community": settings.COMMUNITY})
    else:
        user_form=UserRegistrationForm()
        return render(request,'accounts/register.html', {'user_form':user_form, "community": settings.COMMUNITY})


def home(request):
    return render(request, 'index.html', {"community": settings.COMMUNITY})
